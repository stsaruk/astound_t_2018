(function() {
  'use strict';

  function toggleMenu() {
    let burgerBtn = $('.js-burger-menu');
    let mobileMenu = $('.js-nav');
    let body = $('body');
    let overlay = $('.js-overlay');

    burgerBtn.click(function () {
      burgerBtn.toggleClass('is-open');
      mobileMenu.toggleClass('is-active');
      body.toggleClass('is-fixed');
      overlay.toggleClass('is-active');
    });
  }
  toggleMenu();




  $('.js-slider').slick({
    slidesToScroll: 1,
    slidesToShow: 4,
    infinite: true,
    prevArrow: '<button class="slider__button slider__button--prev"></button>',
    nextArrow: '<button class="slider__button slider__button--next"></button>',
    responsive: [
      {
        breakpoint: 960,
        settings: {
          centerMode: true,
          centerPadding: '0px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 767,
        settings: {
          centerMode: true,
          centerPadding: '0px',
          slidesToShow: 2
        }
      },
      {
        breakpoint: 540,
        settings: {
          centerMode: true,
          centerPadding: '0px',
          slidesToShow: 1
        }
      },
    ]
  });


})();
